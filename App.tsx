/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import ProfileScreen from './src/screens/ProfileScreen/ProfileScreen';

function App(): JSX.Element {
  return <ProfileScreen />;
}

export default App;
