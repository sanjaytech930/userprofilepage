export const Colors = {
  WHITE: 'white',
  BLACK: 'black',
  LIGHT_GREY: 'rgba(230,230,230,0.5)',
};
