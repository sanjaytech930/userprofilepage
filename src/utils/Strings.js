const Strings = {
  MY_NAME: 'my_name',
  POSTS: 'Posts',
  FOLLOWER: 'Follower',
  FOLLOWING: 'Following',
  SIX: '6',
  FOUR_HUN: '400',
  TWO_HUN: '200',
  EDIT_PROFILE: 'Edit profile',
  MY_BIO: 'My bio',
  MY_BIO_2: 'Gamer',
};
export default Strings;
