import React, {useState} from 'react';
import {Image, Text, View} from 'react-native';
import styles from './PostStyle';
function Posts({item}) {
  return (
    <View style={styles.postMainView}>
      {/* post header view */}
      <View style={styles.postHeaderView}>
        <View style={styles.postHeaderImageView}>
          <Image
            style={styles.profileImage}
            source={{uri: item.profile_image}}
          />
        </View>
        <View style={styles.postHeaderTextView}>
          <Text style={styles.postHeaderTextOne}>{item.name}</Text>
          <Text style={styles.postHeaderTextSecond}>{item.location}</Text>
        </View>
      </View>
      {/* post image view */}
      <View style={styles.postImageView}>
        <Image style={styles.postImage} source={{uri: item.post_image}}></Image>
      </View>
    </View>
  );
}
export default Posts;
