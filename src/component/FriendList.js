import React from 'react';
import {Image, Text, View} from 'react-native';
import {StyleSheet} from 'react-native';
import {Colors} from '../utils/Colors';

function FriendList({image, name}) {
  return (
    <View style={styles.view}>
      <View style={styles.imageView}>
        <Image resizeMode="contain" source={image} style={styles.image} />
      </View>
      <Text style={styles.title}>{name}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  view: {alignItems: 'center', marginRight: 20},
  image: {width: 80, height: 80, borderRadius: 100},
  title: {marginTop: 5, color: Colors.WHITE, fontSize: 14},
  imageView: {
    borderWidth: 1,
    padding: 2,
    borderColor: Colors.WHITE,
    borderRadius: 100,
  },
});
export default FriendList;
