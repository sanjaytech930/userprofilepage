import {Dimensions, StyleSheet} from 'react-native';
import {Colors} from '../utils/Colors';

const styles = StyleSheet.create({
  postMainView: {
    marginVertical: 3,
  },
  postHeaderView: {
    flexDirection: 'row',
    marginBottom: 5,
    paddingTop: 5,
    paddingBottom: 5,
  },
  postHeaderImageView: {
    marginLeft: 8,
    marginRight: 8,
    justifyContent: 'center',
  },
  profileImage: {
    width: 30,
    height: 30,
    borderRadius: 30,
  },
  postHeaderTextView: {
    flex: 1,
    justifyContent: 'center',
  },
  postHeaderTextOne: {
    color: Colors.WHITE,
    fontWeight: 'bold',
  },
  postHeaderTextSecond: {
    color: Colors.WHITE,
    fontSize: 11,
  },
  postImage: {
    width: '100%',
    height: Dimensions.get('screen').height / 4,
  },
});
export default styles;
