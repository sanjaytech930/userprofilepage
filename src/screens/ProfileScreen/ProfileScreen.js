import React, {useState} from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import styles from './ProfileScreenStyle';
import Strings from '../../utils/Strings';
import assets from '../../assets/assets';
import PostData from '../../assets/PostData';
import Posts from '../../component/Posts';
import FriendList from '../../component/FriendList';
import Data from '../../assets/Data';
import {launchImageLibrary} from 'react-native-image-picker';

function ProfileScreen(props) {
  const [profileImage, setProfileImage] = useState(null);

  const onPressEdit = () => {
    launchImageLibrary({noData: true}, response => {
      if (response) {
        if (
          response?.assets === undefined ||
          response?.assets === null ||
          response?.assets === ''
        ) {
        } else {
          setProfileImage(response?.assets[0]?.uri);
        }
      }
    });
  };
  return (
    <SafeAreaView style={styles.SafeAreaView}>
      <ScrollView>
        {/* profile header */}
        <View style={styles.profileHeader}>
          <Text style={styles.profileHeaderText}>{Strings.MY_NAME}</Text>
        </View>

        {/* profile header   profile follower view */}
        <View style={styles.profileFollowView}>
          <View style={styles.profileFollowImageView}>
            <Image
              style={styles.profileFollowImage}
              source={
                profileImage === null ? assets.smileImage : {uri: profileImage}
              }
            />
          </View>
          <View style={styles.profileFollowPostView}>
            <Text style={styles.profileFollowText}>{Strings.SIX}</Text>
            <Text style={styles.profileFollowText}>{Strings.POSTS}</Text>
          </View>
          <View style={styles.profileFollowerView}>
            <Text style={styles.profileFollowText}>{Strings.FOUR_HUN}</Text>
            <Text style={styles.profileFollowText}>{Strings.FOLLOWER}</Text>
          </View>
          <View style={styles.profileFollowingPostView}>
            <Text style={styles.profileFollowText}>{Strings.TWO_HUN}</Text>
            <Text style={styles.profileFollowText}>{Strings.FOLLOWING}</Text>
          </View>
        </View>

        {/* profile bio */}
        <View style={styles.headerBioView}>
          <Text style={styles.headerBioText}>{Strings.MY_BIO}</Text>
          <Text style={styles.headerBioText}>{Strings.MY_BIO_2}</Text>
        </View>

        {/* profile button */}
        <View style={styles.profileButtonView}>
          <TouchableOpacity
            style={styles.editProfileView}
            onPress={() => onPressEdit()}>
            <Text style={styles.editProfileText}>{Strings.EDIT_PROFILE}</Text>
          </TouchableOpacity>
        </View>

        {/* friendList */}
        <View style={styles.friendList}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={Data}
            showsVerticalScrollIndicator={false}
            renderItem={({item, index}) => {
              return (
                <FriendList
                  name={item?.name}
                  image={{uri: item?.story_image}}
                />
              );
            }}
            keyExtractor={(item, index) => index}
          />
        </View>
        {/* social post */}
        <View style={styles.postView}>
          <FlatList
            data={PostData}
            renderItem={({item}) => {
              return <Posts item={item} />;
            }}
            scrollEnabled={false}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
export default ProfileScreen;
