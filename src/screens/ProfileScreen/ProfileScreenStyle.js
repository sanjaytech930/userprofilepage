import {Dimensions, StyleSheet} from 'react-native';
import {Colors} from '../../utils/Colors';

const styles = StyleSheet.create({
  SafeAreaView: {
    flex: 1,
    backgroundColor: Colors.BLACK,
  },
  profileHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  profileHeaderText: {
    color: Colors.WHITE,
    width: Dimensions.get('screen').width - 100,
    fontWeight: '700',
    fontSize: 19,
    marginLeft: 15,
  },
  profileFollowView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
  },
  profileFollowImageView: {
    marginLeft: 20,
    marginRight: 25,
  },
  profileFollowImage: {
    height: 70,
    width: 70,
    borderRadius: 40,
  },
  profileFollowText: {
    color: Colors.WHITE,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginLeft: 25,
  },
  profileButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 30,
  },
  editProfileView: {
    borderWidth: 1,
    borderColor: Colors.LIGHT_GREY,
    width: '85%',
    alignItems: 'center',
    padding: 4,
    borderRadius: 5,
  },
  editProfileText: {
    color: Colors.WHITE,
    fontWeight: 'bold',
  },
  friendList: {
    marginVertical: 20,
  },
  headerBioView: {
    marginLeft: 30,
    marginTop: 10,
  },
  headerBioText: {
    color: Colors.WHITE,
    fontWeight: '500',
    fontSize: 15,
  },
});
export default styles;
